using System;
using System.Collections.Generic;
using System.IO;
using ClosedXML.Excel;
using KccNewborn.Entities;
using KccNewborn.Shared;

namespace KccNewborn.Core.Services
{
    public class ExcelService : IExcelService
    {
        public byte[] GenerateDailyReport(IEnumerable<Newborn> data)
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Newborns");
                var currentRow = 1;
                worksheet.Cell(currentRow, 1).Value = "מספר סידורי";
                worksheet.Cell(currentRow, 2).Value = "קוד שובר";
                worksheet.Cell(currentRow, 3).Value = "שם פרטי";
                worksheet.Cell(currentRow, 4).Value = "שם משפחה";
                worksheet.Cell(currentRow, 5).Value = "יישוב";
                worksheet.Cell(currentRow, 6).Value = "רחוב";
                worksheet.Cell(currentRow, 7).Value = "מספר בית";
                worksheet.Cell(currentRow, 8).Value = "מיקוד";
                worksheet.Cell(currentRow, 9).Value = "הערות לשליח";
                worksheet.Cell(currentRow, 10).Value = "טלפון";
                worksheet.Cell(currentRow, 11).Value = "זיהוי הספק";
                worksheet.Cell(currentRow, 12).Value = "שם הספק";
                worksheet.Cell(currentRow, 13).Value = "זיהוי עסקה";
                worksheet.Cell(currentRow, 14).Value = "העסקה";
                worksheet.Cell(currentRow, 15).Value = "תאריך העסקה";
                worksheet.Cell(currentRow, 16).Value = "סטטוס";
                worksheet.Cell(currentRow, 17).Value = "תאריך הסטטוס";
                worksheet.Cell(currentRow, 18).Value = "תאריך תחילת הטיפול";
                worksheet.Cell(currentRow, 19).Value = "מספר ימים בטיפול";
                worksheet.Cell(currentRow, 20).Value = "מספר ימים מאז שנשלח";
                worksheet.Cell(currentRow, 21).Value = "מספר אישור";
                worksheet.Cell(currentRow, 22).Value = "מחיר";
                worksheet.Cell(currentRow, 23).Value = "כמות פריטים";
                worksheet.Cell(currentRow, 24).Value = "SKU";
                worksheet.Cell(currentRow, 25).Value = "Bom Sku";
                worksheet.Cell(currentRow, 26).Value = "קוד איכות כתובת";
                worksheet.Cell(currentRow, 27).Value = "אופן משלוח";
                worksheet.Cell(currentRow, 28).Value = "עלות משלוח";

                var cells = worksheet.Cells();

                foreach (var cell in cells)
                {
                    cell.Style.Font.Bold = true;
                }

                foreach (var item in data)
                {
                    currentRow++;

                    worksheet.Cell(currentRow, 1).Value = item.Id;
                    worksheet.Cell(currentRow, 2).Value = item.EmployeeId.ToString() + item.Id.ToString();
                    worksheet.Cell(currentRow, 3).Value = item.FirstName;
                    worksheet.Cell(currentRow, 4).Value = item.LastName;
                    worksheet.Cell(currentRow, 5).Value = item.City;
                    worksheet.Cell(currentRow, 6).Value = item.Street;
                    worksheet.Cell(currentRow, 7).Value = item.HouseNumber;
                    worksheet.Cell(currentRow, 8).Value = item.ZipCode;
                    worksheet.Cell(currentRow, 9).Value = item.DeliveryNotes;
                    worksheet.Cell(currentRow, 10).Value = item.Phone;
                    worksheet.Cell(currentRow, 11).Value = "777";
                    worksheet.Cell(currentRow, 12).Value = item.CreatedBy;
                    worksheet.Cell(currentRow, 13).Value = "";
                    worksheet.Cell(currentRow, 14).Value = "מתנת לידה 70002";
                    worksheet.Cell(currentRow, 15).Value = item.CreatedDate;
                    worksheet.Cell(currentRow, 16).Value = "מחכה";
                    worksheet.Cell(currentRow, 17).Value = HelperFunctions.GetIsraelDateTimeNow();
                    worksheet.Cell(currentRow, 18).Value = item.CreatedDate;
                    worksheet.Cell(currentRow, 19).Value = "";
                    worksheet.Cell(currentRow, 20).Value = "";
                    worksheet.Cell(currentRow, 21).Value = "";
                    worksheet.Cell(currentRow, 22).Value = "203.8";
                    worksheet.Cell(currentRow, 23).Value = "1";
                    worksheet.Cell(currentRow, 24).Value = "70002";
                    worksheet.Cell(currentRow, 25).Value = "";
                    worksheet.Cell(currentRow, 26).Value = "";
                    worksheet.Cell(currentRow, 27).Value = "523";
                    worksheet.Cell(currentRow, 28).Value = "0";
                }

                using (var stream = new MemoryStream())
                {
                    var columns = worksheet.Columns();
                    foreach (var column in columns)
                    {
                        column.AdjustToContents();
                        column.Width = column.Width + 2;
                    }
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return content;
                }
            }
        }
        public byte[] GenerateExcelReport(IEnumerable<Newborn> data)
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Newborns");
                var currentRow = 1;
                worksheet.Cell(currentRow, 1).Value = "מספר עובד";
                worksheet.Cell(currentRow, 2).Value = "שם פרטי";
                worksheet.Cell(currentRow, 3).Value = "שם משפחה";
                worksheet.Cell(currentRow, 4).Value = "אתר";
                worksheet.Cell(currentRow, 5).Value = "ילד/נכד";
                worksheet.Cell(currentRow, 6).Value = "תאריך לידה";
                worksheet.Cell(currentRow, 7).Value = "יישוב";
                worksheet.Cell(currentRow, 8).Value = "רחוב";
                worksheet.Cell(currentRow, 9).Value = "מספר בית";
                worksheet.Cell(currentRow, 10).Value = "קומה";
                worksheet.Cell(currentRow, 11).Value = "דירה";
                worksheet.Cell(currentRow, 12).Value = "מיקוד";
                worksheet.Cell(currentRow, 13).Value = "הערות לשליח";
                worksheet.Cell(currentRow, 14).Value = "תאריך הזנה";
                worksheet.Cell(currentRow, 15).Value = "הוזן ע\"י";

                var cells = worksheet.Cells();

                foreach (var cell in cells)
                {
                    cell.Style.Font.Bold = true;
                }

                foreach (var item in data)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = item.EmployeeId;
                    worksheet.Cell(currentRow, 2).Value = item.FirstName;
                    worksheet.Cell(currentRow, 3).Value = item.LastName;
                    worksheet.Cell(currentRow, 4).Value = HelperFunctions.GetEnumDescription(item.Site);
                    worksheet.Cell(currentRow, 5).Value = HelperFunctions.GetEnumDescription(item.Type);
                    worksheet.Cell(currentRow, 6).Value = item.DateOfBirth;
                    worksheet.Cell(currentRow, 7).Value = item.City;
                    worksheet.Cell(currentRow, 8).Value = item.Street;
                    worksheet.Cell(currentRow, 9).Value = item.HouseNumber;
                    worksheet.Cell(currentRow, 10).Value = item.Floor;
                    worksheet.Cell(currentRow, 11).Value = item.ApartmentNumber;
                    worksheet.Cell(currentRow, 12).Value = item.ZipCode;
                    worksheet.Cell(currentRow, 13).Value = item.DeliveryNotes;
                    worksheet.Cell(currentRow, 14).Value = item.CreatedDate;
                    worksheet.Cell(currentRow, 15).Value = item.CreatedBy;
                }

                using (var stream = new MemoryStream())
                {
                    var columns = worksheet.Columns();
                    foreach (var column in columns)
                    {
                        column.AdjustToContents();
                        column.Width = column.Width + 2;
                    }
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return content;
                }
            }
        }

        public string GenerateFileName(NewbornParameters parameters)
        {
            string fileName = $"newborns_{parameters.MinDateOfBirth.ToString("dd-MM-yyyy")}_{parameters.MaxDateOfBirth.ToString("dd-MM-yyyy")}";

            // if (!string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate))
            // {
            //     fileName = $"newborns_{DateTime.Parse(startDate).ToString("dd-MM-yyyy")}_to_{DateTime.Parse(endDate).ToString("dd-MM-yyyy")}.xlsx";
            // }
            // else if (!string.IsNullOrEmpty(startDate))
            // {
            //     fileName = $"newborns_{DateTime.Parse(startDate).ToString("dd-MM-yyyy")}_{HelperFunctions.GetIsraelDateNow().ToString("dd-MM-yyyy")}.xlsx";
            // }
            // else if (!string.IsNullOrEmpty(endDate))
            // {
            //     fileName = $"newborns_all_until_{DateTime.Parse(endDate).ToString("dd-MM-yyyy")}.xlsx";
            // }
            // else
            // {
            //     fileName = "newborns_all.xlsx";
            // }

            return fileName;
        }
    }
}