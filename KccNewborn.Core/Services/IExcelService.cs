using System.Collections.Generic;
using KccNewborn.Entities;

namespace KccNewborn.Core.Services
{
    public interface IExcelService
    {
        byte[] GenerateDailyReport(IEnumerable<Newborn> data);

        byte[] GenerateExcelReport(IEnumerable<Newborn> data);

        string GenerateFileName(NewbornParameters parameters);
    }
}