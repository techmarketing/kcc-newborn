using System.IO;
using System.Threading.Tasks;

namespace KccNewborn.Core.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string content);
        Task SendEmailAsync(string email, string subject, string message, byte[] file, string fileName);
    }
}