using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace KccNewborn.Core.Services
{
    public class EmailSender : IEmailSender
    {
        private readonly EmailSettings _emailSettings;

        public EmailSender(EmailSettings emailSettings)
        {
            _emailSettings = emailSettings;
        }

        public async Task SendEmailAsync(string email, string subject, string content)
        {
            try
            {
                var message = CreateEmailMessage(email, subject, content);

                using (SmtpClient smtp = new SmtpClient(_emailSettings.SmtpServer, _emailSettings.Port))
                {
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                    if (!String.IsNullOrEmpty(_emailSettings.Username) && !String.IsNullOrEmpty(_emailSettings.Password))
                    {
                        smtp.Credentials = new NetworkCredential(_emailSettings.Username, _emailSettings.Password);
                    }

                    smtp.EnableSsl = _emailSettings.EnableSsl;

                    await smtp.SendMailAsync(message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task SendEmailAsync(string email, string subject, string content, byte[] file, string fileName)
        {
            try
            {
                var message = CreateEmailMessage(email, subject, content);

                using (Stream stream = new MemoryStream(file))
                {
                    var attachment = new Attachment(stream, fileName);
                    message.Attachments.Add(attachment);

                    using (SmtpClient smtp = new SmtpClient(_emailSettings.SmtpServer, _emailSettings.Port))
                    {
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                        if (!String.IsNullOrEmpty(_emailSettings.Username) && !String.IsNullOrEmpty(_emailSettings.Password))
                        {
                            smtp.Credentials = new NetworkCredential(_emailSettings.Username, _emailSettings.Password);
                        }

                        smtp.EnableSsl = true;

                        await smtp.SendMailAsync(message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private MailMessage CreateEmailMessage(string email, string subject, string content)
        {
            try
            {
                MailMessage message = new MailMessage()
                {
                    From = new MailAddress(_emailSettings.Address, _emailSettings.Name),
                    Subject = subject,
                    Body = content,
                    IsBodyHtml = true
                };
                message.To.Clear();
                char[] delimiters = { ';', ',' };
                var emails = email.Split(delimiters);
                foreach (string single in emails)
                {
                    message.To.Add(new MailAddress(single.Trim(), single.Trim()));
                }

                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}