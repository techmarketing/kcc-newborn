using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using KccNewborn.DAL;
using KccNewborn.Entities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Linq;
using KccNewborn.Shared;
using Microsoft.EntityFrameworkCore;

namespace KccNewborn.Core.Services
{
    public class JobRunner : IHostedService, IDisposable
    {
        private readonly IServiceScopeFactory scopeFactory;
        private readonly EmailSettings _emailSettings;

        public JobRunner(IServiceScopeFactory scopeFactory, EmailSettings emailSettings)
        {
            _emailSettings = emailSettings;
            this.scopeFactory = scopeFactory;
        }

        private Timer _timer;

        public void Dispose()
        {
            this._timer?.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            TimeSpan interval = TimeSpan.FromHours(24);

            var nextRunTime = HelperFunctions.GetIsraelDateNow().AddDays(1).AddHours(1);
            var currentTime = HelperFunctions.GetIsraelDateTimeNow();
            var firstInterval = nextRunTime.Subtract(currentTime);

            _timer = new Timer(SendDailyReport, null, firstInterval, interval);

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        private async void SendDailyReport(object state)
        {
            if (!_emailSettings.DailyReportActivated)
            {
                return;
            }

            using (var scope = scopeFactory.CreateScope())
            {
                var repo = scope.ServiceProvider.GetRequiredService<IRepository>();

                Guid g = Guid.NewGuid();
                string identifierString = Convert.ToBase64String(g.ToByteArray());

                var identifier = new DailyReport() { Date = DateTime.Now.Date, Identifier = identifierString };

                try
                {
                    repo.Add<DailyReport>(identifier);

                    await repo.SaveAll();
                }
                catch (DbUpdateException ex)
                {
                    return;
                }

                Thread.Sleep(5000);

                if (await repo.IsEntryExists(identifierString) == false)
                {
                    return;
                }

                var data = await repo.GetNewbornsForDailyReport();

                if (data != null)
                {
                    var excelService = scope.ServiceProvider.GetRequiredService<IExcelService>();
                    var emailSender = scope.ServiceProvider.GetRequiredService<IEmailSender>();
                    var file = excelService.GenerateDailyReport(data);
                    var yesterday = HelperFunctions.GetIsraelDateNow().AddDays(-1).ToString("dd-MM-yyyy");

                    await emailSender.SendEmailAsync(
                        _emailSettings.Recipients,
                        $"דו\"ח ניובורן יומי - {yesterday}",
                        $"מצורף דו\"ח יומי לתאריך {yesterday}",
                        file,
                        $"newborn_daily_{yesterday}.xlsx"
                    );
                }
            }


        }
    }
}