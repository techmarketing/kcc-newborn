namespace KccNewborn.Core
{
    public class EmailSettings
    {
        public string SmtpServer { get; set; }
        public bool EnableSsl { get; set; }
        public int Port { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Recipients { get; set; }
        public bool DailyReportActivated { get; set; }
    }
}