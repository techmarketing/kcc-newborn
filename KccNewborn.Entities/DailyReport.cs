using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using KccNewborn.Shared;

namespace KccNewborn.Entities
{
    public class DailyReport
    {
        [Key]
        [DataType(DataType.Date)]
        [Column(TypeName = "Date")]
        public DateTime Date { get; set; }

        [Required]
        public string Identifier { get; set; }
    }
}
