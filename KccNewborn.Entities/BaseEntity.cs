using System;

namespace KccNewborn.Entities
{
    public class BaseEntity
    {
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }
        
        public string CreatedBy { get; set; }
    }
}