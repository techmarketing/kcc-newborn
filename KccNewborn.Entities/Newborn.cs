﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using KccNewborn.Shared;

namespace KccNewborn.Entities
{
    public class Newborn : BaseEntity
    {
        private string _firstName;

        [Required]
        [StringLength(100)]
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = HelperFunctions.Sanitize(value); }
        }

        private string _lastName;

        [Required]
        [StringLength(100)]
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = HelperFunctions.Sanitize(value); }
        }

        [Required]
        [StringLength(7, MinimumLength = 7)]
        [RegularExpression(@"^14\d{5}$")]
        public string EmployeeId { get; set; }

        [Required]
        [MinLength(9)]
        [MaxLength(10)]
        [RegularExpression(@"^0((?:[57]\d|[2489])|(?:[23489]))-?[2-9]\d{6}$")]
        public string Phone { get; set; }

        [Required]
        public SiteNames Site { get; set; }

        [Required]
        public NewbornTypes Type { get; set; }

        private DateTime _dateOfBirth;
        [Required]
        [DataType(DataType.Date)]
        [Column(TypeName = "Date")]
        public DateTime DateOfBirth
        {
            get { return _dateOfBirth; }
            set
            {
                TimeZoneInfo israelZone = TimeZoneInfo.FindSystemTimeZoneById("Israel Standard Time");
                _dateOfBirth = TimeZoneInfo.ConvertTimeFromUtc(value, israelZone);
            }
        }


        [Required]
        [StringLength(100)]
        public string City { get; set; }

        [Required]
        [StringLength(100)]
        public string Street { get; set; }

        [Required]
        [StringLength(100)]
        public string HouseNumber { get; set; }

        public int? Floor { get; set; }

        public int? ApartmentNumber { get; set; }

        [StringLength(7)]
        public string ZipCode { get; set; }

        [StringLength(500)]
        public string DeliveryNotes { get; set; }

    }

    public enum SiteNames
    {
        [EnumMember(Value = "נהריה")]
        [Description("נהריה")]
        Nahariya = 0,

        [EnumMember(Value = "חדרה")]
        [Description("חדרה")]
        Hadera = 1,

        [EnumMember(Value = "עפולה")]
        [Description("עפולה")]
        Afula = 2,

        [EnumMember(Value = "צריפין")]
        [Description("צריפין")]
        Zrifin = 3,

        [EnumMember(Value = "לוגיסטיקה צפון")]
        [Description("לוגיסטיקה צפון")]
        LogisticsNorth = 4
    }

    public enum NewbornTypes
    {
        [EnumMember(Value = "ילד")]
        [Description("ילד")]
        Child = 0,

        [EnumMember(Value = "נכד")]
        [Description("נכד")]
        Grandchild = 1
    }
}
