﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using KccNewborn.Shared;

namespace KccNewborn.Entities
{
    public class User : BaseEntity
    {
        private string _firstName;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = HelperFunctions.Sanitize(value); }
        }

        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = HelperFunctions.Sanitize(value); }
        }

        [EmailAddress]
        public string Email { get; set; }

        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
    }
}
