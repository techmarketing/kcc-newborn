using System.ComponentModel.DataAnnotations;

namespace KccNewborn.Entities.Dtos
{
    public class UserForListDto
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        private string _fullName;
        public string FullName
        {
            get
            {
                return $"{FirstName} + {LastName}";
            }
            set
            {
                _fullName = value;
            }
        }

        [EmailAddress]
        public string Email { get; set; }
    }
}