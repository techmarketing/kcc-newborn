using System.ComponentModel.DataAnnotations;

namespace KccNewborn.Entities.Dtos
{
    public class UserForLoginDto
    {
        [EmailAddress]
        public string Email { get; set; }
        public string Password { get; set; }
    }
}