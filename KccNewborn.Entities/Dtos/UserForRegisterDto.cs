using System.ComponentModel.DataAnnotations;

namespace KccNewborn.Entities.Dtos
{
    public class UserForRegisterDto
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [RegularExpression(@"^14\d{5}$")]
        public string Password { get; set; }
    }
}