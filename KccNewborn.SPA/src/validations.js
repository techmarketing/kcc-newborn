export const isName = (value) => {
    const pattern = /^\s*[a-zא-ת][a-zא-ת`\']+([\-\s][a-zא-ת]+)*\s*$/i;

    if (!value || value === "") {
        return true;
    } else {
        return value.toString().match(pattern);
    }
};

export const isFullName = (value) => {
    const pattern = /^(\s)*[a-zא-ת][a-zא-ת\-\׳\"]+(?:(\s)+[a-zא-ת][a-zא-ת\-\׳\"]+)+(\s)*$/i;
    return value.toString().match(pattern);
};

export const isEmail = (value) => {
    const pattern = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    return value.toString().match(pattern);
};

export const isPhone = (value) => {
    const pattern = /^0((?:[57]\d|[2489])|(?:[23489]))-?[2-9]\d{6}$/;
    return value.toString().match(pattern);
}

export const isEmployeeId = (value) => {
    const pattern = /^14\d{5}/i;
    return value.toString().match(pattern);
}

export const isNumber = (value) => {
    const pattern = /^[0-9]*$/;
    return value.toString().match(pattern);
}

export const isZipCode = (value) => {
    const pattern = /^[0-9]{7}/;
    
    if (!value || value === "") {
        return true;
    }

    return value.toString().match(pattern);
}
