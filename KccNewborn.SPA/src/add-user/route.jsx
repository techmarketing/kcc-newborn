import React, { useState } from 'react';

import AddUser from './AddUser';
import AuthService from '../services/auth.service';

export const path = '/users/add';
export const exact = true;

export const Component = (props) => {
  const [isSending, setIsSending] = useState(null);

  const [showNotification, setShowNotification] = useState(false);
  const [notificationText, setNotificationText] = useState(null);

  const handleCloseNotification = () => {
    setShowNotification(false);
  };

  const onSubmit = async ({ firstName, lastName, email, password }, e) => {
    try {
      setIsSending(true);
      const response = await AuthService.register(
        firstName,
        lastName,
        email,
        password
      );
      setIsSending(false);

      if (response.status === 201) {
        setNotificationText({
          title: 'הוספת משתמש',
          text: 'משתמש נוצר בהצלחה',
        });

        e.target.reset();
        setShowNotification(true);
      } else if (response.status === 400) {
        setNotificationText({
          title: 'הוספת משתמש',
          text: `כתובת דוא"ל זו כבר קיימת במערכת`,
        });
        setShowNotification(true);
      }
    } catch (e) {
      setNotificationText({
        title: 'הוספת משתמש',
        text: `אירעה שגיאה. אנא נסה שוב.`,
      });
      setShowNotification(true);
      setIsSending(false);
    }
  };

  return (
    <AddUser
      onSubmit={onSubmit}
      isSending={isSending}
      showNotification={showNotification}
      notificationText={notificationText}
      handleCloseNotification={handleCloseNotification}
    />
  );
};
