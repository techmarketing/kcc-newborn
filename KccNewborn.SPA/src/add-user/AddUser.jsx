import React, { useState } from 'react';
import { useForm } from 'react-hook-form';

import Form from '../components/Form';
import Input from '../components/Input';
import * as Validations from '../validations';

import './AddUser.scss';
import Notification from '../components/Notification';

const AddUser = ({
  onSubmit,
  isSending = false,
  notificationText,
  showNotification = false,
  handleCloseNotification,
}) => {
  const { register, handleSubmit, errors, watch } = useForm();

  return (
    <div id="add_user" className="container">
      <div className="inner-wrapper">
        <h2 className="mb-5">הוספת משתמש חדש</h2>
        <Form
          isSending={isSending}
          onSubmit={handleSubmit(onSubmit)}
          btnText="הוסף משתמש"
        >
          <div className="inputs-container mb-4">
            <div className="form-row">
              <div className="col-md-6">
                <Input
                  className=""
                  type="text"
                  label="שם פרטי *"
                  name="firstName"
                  placeholder=""
                  ref={register({
                    required: {
                      value: true,
                      message: 'שדה חובה'
                    },
                    validate: (value) =>
                      Validations.isName(value) || 'שם פרטי לא תקין',
                  })}
                  error={errors.firstName && errors.firstName.message}
                />
              </div>
              <div className="col-md-6">
                <Input
                  className=""
                  type="text"
                  label="שם משפחה *"
                  name="lastName"
                  placeholder=""
                  ref={register({
                    required: {
                      value: true,
                      message: 'שדה חובה'
                    },
                    validate: (value) =>
                      Validations.isName(value) || 'שם משפחה לא תקין',
                  })}
                  error={errors.lastName && errors.lastName.message}
                />
              </div>
            </div>

            <div className="form-row">
              <div className="col-md-12">
                <Input
                  className=""
                  type="text"
                  label={`כתובת דוא"ל *`}
                  name="email"
                  placeholder=""
                  ref={register({
                    required: {
                      value: true,
                      message: 'שדה חובה',
                    },
                    validate: (value) =>
                      Validations.isEmail(value) || `כתובת דוא"ל לא תקינה`,
                  })}
                  error={errors.email && errors.email.message}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="col-md-12">
                <Input
                  className=""
                  type="password"
                  label="סיסמה *"
                  name="password"
                  maxLength="8"
                  placeholder=""
                  ref={register({
                    required: {
                      value: true,
                      message: 'שדה חובה',
                    },
                    validate: (value) =>
                      Validations.isEmployeeId(value) ||
                      'סיסמה צריכה להיות בפורמט מספר עובד',
                  })}
                  error={errors.password && errors.password.message}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="col-md-12">
                <Input
                  className=""
                  type="password"
                  label="אימות סיסמה *"
                  name="passwordValidate"
                  maxLength="8"
                  placeholder=""
                  ref={register({
                    required: {
                      value: true,
                      message: 'שדה חובה',
                    },
                    validate: (value) =>
                      value === watch('password') ||
                      'הערך לא תואם את הסיסמה הראשונה',
                  })}
                  error={
                    errors.passwordValidate && errors.passwordValidate.message
                  }
                />
              </div>
            </div>
          </div>
        </Form>
      </div>
      <Notification
        show={showNotification}
        handleClose={handleCloseNotification}
        content={notificationText}
      />
    </div>
  );
};

export default AddUser;
