import Axios from 'axios';
import { history } from 'react-router-dom';
import authHeader from "./auth-header";

const API_URL = "/api/auth";

Axios.interceptors.response.use(response => {
    return response;
}, error => {
    if (error.response.status === 401) {
        if (error.response.data === "LOGIN") {
            return Promise.reject(error);
        } else {
            logout();
            location.reload();
        }
    }

    return Promise.reject(error);
});

const register = async (firstName, lastName, email, password) => {
    try {
        return await Axios.post(`${API_URL}/register`, { firstName, lastName, email, password }, { headers: authHeader() });
    } catch (error) {
        return error.response;
    }
};

const login = (email, password) => {

    return Axios.post(`${API_URL}/login`, { email, password })
        .then((response) => {
            if (response.data && response.data.token) {
                localStorage.setItem("user", JSON.stringify(response.data));
            }
            return response.data;
        });
};

const logout = () => {
    localStorage.removeItem("user");
};

const getCurrentUser = () => {
    return JSON.parse(localStorage.getItem("user"));
};

export default {
    register,
    login,
    logout,
    getCurrentUser,
};