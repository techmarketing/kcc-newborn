import Axios from "axios";
import authHeader from "./auth-header";
import FileDownload from 'js-file-download';

const API_URL = "/api/excel";

const quarterly = async () => {
    try {
        const response = await Axios.get(`${API_URL}/quarterly`, { headers: authHeader(), responseType: 'blob' });
        createFileDownload(response);
    } catch (error) {
        return error.response;
    }
}

const getByDates = async (minDate = null, maxDate = null) => {
    const query = `${minDate ? '&minDateOfBirth=' : ''}${minDate || ''}${maxDate ? '&maxDateOfBirth=' : ''}${maxDate || ''}`;
    try {
        const response = await Axios.get(`${API_URL}${query ? '?' : ''}${query}`, { headers: authHeader(), responseType: 'blob' });
        createFileDownload(response);
    } catch (error) {
        return error.response;
    }
}

const createFileDownload = (response, minDate, maxDate) => {
    const contentType = response.headers['content-type'] || '';

    if (contentType === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
        return FileDownload(response.data, `report_${new Date().toLocaleDateString("he-IL").replace(/[.]/g, '-')}.xlsx`)
    }
}

export default {
    quarterly,
    getByDates
};