import Axios from "axios";
import authHeader from "./auth-header";

const API_URL = "/api/newborns";

const getAll = async () => {
    try {
        return await Axios.get(API_URL, { headers: authHeader() });
    } catch (error) {
        return error.response;
    }
};

const getPagedData = async (pageNumber = 1, pageSize = 10, minDate = null, maxDate = null) => {
    const query = `?pageNumber=${pageNumber}&pageSize=${pageSize}${minDate ? '&minDateOfBirth=' : ''}${minDate || ''}${maxDate ? '&maxDateOfBirth=' : ''}${maxDate || ''}`;
    try {
        return await Axios.get(`${API_URL}${query}`, { headers: authHeader() });
    } catch (error) {
        return error.response;
    }
}

const create = async (data) => {
    try {
        return await Axios.post(API_URL, data, { headers: authHeader() });
    } catch (error) {
        return error.response;
    }
};

export default {
    getAll,
    getPagedData,
    create
};