import React, { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';

import Form from '../components/Form';
import Input from '../components/Input';
import * as Validations from '../validations';
import AuthService from '../services/auth.service';
import { withRouter, Redirect } from 'react-router-dom';
import Logo from '../components/Logo';
import './Login.scss';
import { Alert } from 'react-bootstrap';

const Login = ({ history, updateCurrentUserState }) => {
  const { register, handleSubmit, errors } = useForm();
  const [isSending, setIsSending] = useState(null);
  const [isFailed, setIsFailed] = useState(false);

  useEffect(() => {
    updateCurrentUserState();
  }, []);

  const onSubmit = async ({ email, password }) => {
    try {
      setIsSending(true);
      const result = await AuthService.login(email, password);
      setIsSending(false);

      if (result && history) {
        updateCurrentUserState();
        history.push({
          pathname: '/newborns',
        });
      } else {
        setIsFailed(true);
      }
    } catch (error) {
      setIsSending(false);
      setIsFailed(true);
    }
  };

  if (AuthService.getCurrentUser()) {
    return <Redirect to="/newborns" />;
  }

  return (
    <div id="login_page" className="text-center container">
      <Form
        className="form-signin"
        isSending={isSending}
        onSubmit={handleSubmit(onSubmit)}
        btnText="כניסה למערכת"
      >
        <div className="logo mb-4">
          <Logo />
        </div>
        <div className="inputs mb-4">
          <Input
            type="email"
            // label={`כתובת דוא"ל`}
            name="email"
            placeholder={`כתובת דוא"ל`}
            onChange={() => setIsFailed(false)}
            ref={register({
              required: {
                value: true,
                message: 'שדה חובה',
              },
              validate: (value) =>
                Validations.isEmail(value) || `כתובת דוא"ל לא תקינה`,
            })}
            error={errors.email && errors.email.message}
          />

          <Input
            type="password"
            // label="סיסמה"
            name="password"
            placeholder="סיסמה"
            onChange={() => setIsFailed(false)}
            ref={register({
              required: {
                value: true,
                message: 'שדה חובה',
              },
            })}
            error={errors.password && errors.password.message}
          />
        </div>
        {isFailed && (
          <Alert className="alert" variant="danger">
            שם משתמש או סיסמה לא נכונים
          </Alert>
        )}
      </Form>
    </div>
  );
};

export default withRouter(Login);
