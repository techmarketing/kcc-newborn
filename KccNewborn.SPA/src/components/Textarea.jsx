import React, { forwardRef } from 'react';

const Textarea = forwardRef(
  ({ label, placeholder, error, className, ...props }, ref) => (
    <div className={`${className}`}>
      <label className="">{label}</label>
      <textarea className="form-control" {...props} rows="3" ref={ref}></textarea>
    </div>
  )
);

export default Textarea;
