import React from 'react';
import LogoImg from "../images/logo.png";

const Logo = () => <img src={LogoImg} alt="לוגו קימברלי-קלארק" />;

export default Logo;