import React from 'react';
import { Modal, Button } from 'react-bootstrap';

const Notification = ({ show = false, handleClose, content }) => {
    
  return (
    <>
      <Modal
        size="sm"
        show={show}
        onHide={handleClose}
      >
        <Modal.Header closeButton>
          <Modal.Title>
            {content && content.title}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ direction: 'rtl' }}>{content && content.text}</Modal.Body>
        <Modal.Footer style={{ direction: 'rtl' }}>
        <Button onClick={handleClose}>סגור</Button>
      </Modal.Footer>
      </Modal>
    </>
  );
};

export default Notification;
