import React from 'react';
import { Button } from 'react-bootstrap';

const Form = ({ children, onSubmit, isSending = false, btnText, btnClassName, ...props }) => {
  return (
    <form
      {...props}
      onSubmit={(e) => {
        e.preventDefault();
        onSubmit(e);
      }}
    >
      {children}

      <Button variant="primary" disabled={isSending} type="submit" className={btnClassName}>{btnText}</Button>
    </form>
  );
};

export default Form;
