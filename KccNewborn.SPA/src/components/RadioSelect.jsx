import React, { forwardRef } from 'react';
import './Input.scss';

const RadioSelect = forwardRef(
  ({ label, options, placeholder, error, className, ...props }, ref) => (
    <div className={`input-wrapper mb-3 ${className}`}>
      <label style={{display: 'block'}}>{label}</label>
      {options.map((value) => (
        <div key={value} className="form-check form-check-inline">
          <label className="form-check-label">
            <input
              ref={ref}
              {...props}
              type="radio"
              value={value}
              className="form-check-input"
            />
            {value}
          </label>
        </div>
      ))}
      {error && (
        <div className="error">
          * {error}
        </div>
      )}
    </div>
  )
);

export default RadioSelect;
