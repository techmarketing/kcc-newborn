import React, { forwardRef } from 'react';
import './Input.scss';

const Input = forwardRef(
  ({ label, placeholder, error, className, ...props }, ref) => (
    <div className="input-wrapper mb-4">
      { label && <label>{label}</label>}
      <input
        ref={ref}
        placeholder={placeholder}
        {...props}
        className={`form-control ${error && 'input-error'}`}
      />
      {error && <div className="error">* {error}</div>}
    </div>
  )
);

export default Input;
