import React, { useState, useRef } from 'react';
import { Link } from 'react-router-dom';

import './Sidebar.scss';
import Logo from './Logo';
import { Spinner } from 'react-bootstrap';

import ExcelService from '../services/excel.service';

const Sidebar = React.forwardRef((props, ref) => {
  const [isLoading, setIsLoading] = useState(false);

  const handleReportClick = async () => {
    setIsLoading(true);
    await ExcelService.quarterly();
    setIsLoading(false);
    handleClick();
  };

  const handleClick = () => {
    if (ref.current) {
      ref.current.classList.remove('show');
    }
  }

  return (
    <>
      <div id="sidebar" ref={ref}>
        <div className="bg-light border-left" id="sidebar-wrapper">
          <div className="sidebar-heading pt-5 pb-5">
            <Logo />
          </div>
          <div className="list-group list-group-flush">
            <Link
              className="list-group-item list-group-item-action bg-light"
              to="/newborns"
              onClick={handleClick}
            >
              רשומות ניובורן
            </Link>
            <Link
              className="list-group-item list-group-item-action bg-light"
              to="/newborns/add"
              onClick={handleClick}
            >
              הוספת רשומה חדשה
            </Link>
            <Link
              className="list-group-item list-group-item-action bg-light"
              to="/users/add"
              onClick={handleClick}
            >
              הוספת משתמש חדש
            </Link>
            <div
              onClick={handleReportClick}
              id="report"
              className="list-group-item list-group-item-action bg-light"
            >
              <span>הוצאת דו"ח רבעוני</span>
              {isLoading && (
                <Spinner animation="border" variant="primary" size="sm" />
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
});

export default Sidebar;
