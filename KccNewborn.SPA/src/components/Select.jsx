import React, { forwardRef } from 'react';
import './Input.scss';

const Input = forwardRef(
  ({ label, options, placeholder, error, className, ...props }, ref) => (
    <div className="input-wrapper mb-4">
      <label className="">{label}</label>
      <select
        ref={ref}
        placeholder={placeholder}
        {...props}
        className={`form-control ${error && 'input-error'}`}
      >
        <option value="" />
        {options.map((value) => (
          <option key={value} value={value}>
            {value}
          </option>
        ))}
      </select>
      {error && (
        <div className="error">
          * {error}
        </div>
      )}
    </div>
  )
);

export default Input;
