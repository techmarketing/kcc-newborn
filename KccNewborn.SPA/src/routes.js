import * as notFound from './not-found/route';
import * as addUser from './add-user/route';
import * as newborns from './newborns/data/route';
import * as newbornsForm from './newborns/form/route';

export default [addUser, newbornsForm, newborns, notFound];
