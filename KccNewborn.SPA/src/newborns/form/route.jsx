import React, { useState } from 'react';

export const path = '/newborns/add';
export const exact = true;

import NewbornForm from './NewbornForm';
import NewbornsService from '../../services/newborns.service';

export const Component = (props) => {
  const [isSending, setIsSending] = useState(false);

  const [showNotification, setShowNotification] = useState(false);
  const [notificationText, setNotificationText] = useState(null);

  const handleCloseNotification = () => {
    setShowNotification(false);
  };

  const onSubmit = async (
    {
      firstName,
      lastName,
      employeeId,
      phone,
      site,
      type,
      dateOfBirth,
      city,
      street,
      houseNumber,
      floor,
      apartmentNumber,
      zipCode,
      deliveryNotes,
    },
    e
  ) => {
    const formData = {
      firstName,
      lastName,
      employeeId,
      phone,
      site,
      type,
      dateOfBirth,
      city,
      street,
      houseNumber,
      floor,
      apartmentNumber,
      zipCode,
      deliveryNotes,
    };
    try {
      setIsSending(true);
      const response = await NewbornsService.create(formData);
      setIsSending(false);

      if (response.status === 200) {
        setNotificationText({
          title: 'הוספת רשומה',
          text: 'רשומה נוצרה בהצלחה',
        });

        setShowNotification(true);
        e.target.reset();
      } else {
        setNotificationText({
          title: 'הוספת רשומה',
          text: 'אירעה שגיאה. אנא נסה שוב.',
        });

        setShowNotification(true);
      }
    } catch (error) {
      setNotificationText({
        title: 'הוספת רשומה',
        text: `אירעה שגיאה. אנא נסה שוב.`,
      });
      setShowNotification(true);
      setIsSending(false);
    }
  };

  return (
    <NewbornForm
      onSubmit={onSubmit}
      isSending={isSending}
      showNotification={showNotification}
      notificationText={notificationText}
      handleCloseNotification={handleCloseNotification}
    />
  );
};
