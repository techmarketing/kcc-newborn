import React, { useState, useRef, useEffect } from 'react';
import Input from '../../components/Input';
import RadioSelect from '../../components/RadioSelect';
import Textarea from '../../components/Textarea';
import Select from '../../components/Select';
import { useForm, Controller } from 'react-hook-form';
import * as Validations from '../../validations';
import Form from '../../components/Form';
import './NewbornForm.scss';
import ReactDatePicker, { registerLocale } from 'react-datepicker';
import he from 'date-fns/locale/he';
import Notification from '../../components/Notification';
registerLocale('he', he);

import 'react-datepicker/dist/react-datepicker.css';

const NewbornForm = ({
  onSubmit,
  isSending = false,
  notificationText,
  showNotification = false,
  handleCloseNotification,
}) => {
  const { register, handleSubmit, errors, control } = useForm();

  return (
    <div id="newborn_form" className="container">
      <h2 className="mb-5">הוספת רשומה חדשה</h2>
      <Form
        onSubmit={handleSubmit(onSubmit)}
        btnText="הוסף רשומה"
        isSending={isSending}
      >
        <div className="employee-details mb-3">
          <h4 className="mb-3">פרטי העובד</h4>
          <div className="form-row">
            <div className="col-md-6">
              <Input
                className=""
                type="text"
                label="שם פרטי *"
                name="firstName"
                placeholder=""
                ref={register({
                  required: {
                    value: true,
                    message: 'שדה חובה',
                  },
                  validate: (value) =>
                    Validations.isName(value) || 'שם פרטי לא תקין',
                })}
                error={errors.firstName && errors.firstName.message}
              />
            </div>
            <div className="col-md-6">
              <Input
                className=""
                type="text"
                label="שם משפחה *"
                name="lastName"
                placeholder=""
                ref={register({
                  required: {
                    value: true,
                    message: 'שדה חובה',
                  },
                  validate: (value) =>
                    Validations.isName(value) || 'שם משפחה לא תקין',
                })}
                error={errors.lastName && errors.lastName.message}
              />
            </div>
          </div>
          <div className="form-row">
            <div className="col-md-4">
              <Input
                className=""
                type="text"
                label="מספר עובד *"
                name="employeeId"
                maxLength="7"
                placeholder=""
                ref={register({
                  required: {
                    value: true,
                    message: 'שדה חובה',
                  },
                  validate: (value) =>
                    Validations.isEmployeeId(value) || 'מספר עובד לא תקין',
                })}
                error={errors.employeeId && errors.employeeId.message}
              />
            </div>
            <div className="col-md-4">
              <Input
                className=""
                type="text"
                label="טלפון *"
                name="phone"
                maxLength="10"
                placeholder=""
                ref={register({
                  required: {
                    value: true,
                    message: 'שדה חובה',
                  },
                  validate: (value) =>
                    Validations.isPhone(value) || 'מספר טלפון לא תקין',
                })}
                error={errors.phone && errors.phone.message}
              />
            </div>
            <div className="col-md-4">
              <Select
                className=""
                options={['נהריה', 'חדרה', 'עפולה', 'צריפין', 'לוגיסטיקה צפון']}
                label="אתר *"
                name="site"
                placeholder=""
                ref={register({
                  required: {
                    value: true,
                    message: 'שדה חובה',
                  },
                })}
                error={errors.site && errors.site.message}
              />
            </div>
          </div>
        </div>

        <div className="child-details mb-3">
          <h4 className="mb-3">פרטי הפעוט</h4>
          <div className="form-row">
            <div className="col-md-6 col-lg-4 mb-3">
              <label>תאריך לידה *</label>
              <Controller
                name="dateOfBirth"
                defaultValue=""
                control={control}
                rules={{ required: { value: true, message: 'שדה חובה' } }}
                render={({ onChange, onBlur, value }) => (
                  <ReactDatePicker
                    name="dateOfBirth"
                    locale="he"
                    // dateFormat="P"
                    dateFormat="dd/MM/yyyy"
                    className={`form-control ${
                      errors.dateOfBirth && 'input-error'
                    }`}
                    onChange={onChange}
                    onBlur={onBlur}
                    selected={value}
                    maxDate={new Date()}
                  />
                )}
              />
              {errors.dateOfBirth && (
                <div className="error">* {errors.dateOfBirth.message}</div>
              )}
            </div>

            <div className="col-md-6 col-lg-4 mb-3">
              <RadioSelect
                className=""
                label="ילד/נכד *"
                options={['ילד', 'נכד']}
                name="type"
                ref={register({
                  required: {
                    value: true,
                    message: 'שדה חובה',
                  },
                })}
                error={errors.type && errors.type.message}
              />
            </div>
          </div>
        </div>

        <div className="delivery-details mb-4">
          <h4 className="mb-3">פרטי משלוח</h4>
          <div className="form-row">
            <div className="col-md-5">
              <Input
                className=""
                type="text"
                label="יישוב *"
                name="city"
                placeholder=""
                ref={register({
                  required: {
                    value: true,
                    message: 'שדה חובה',
                  },
                })}
                error={errors.city && errors.city.message}
              />
            </div>
            <div className="col-md-5">
              <Input
                className=""
                type="text"
                label="רחוב *"
                name="street"
                placeholder=""
                ref={register({
                  required: {
                    value: true,
                    message: 'שדה חובה',
                  },
                })}
                error={errors.street && errors.street.message}
              />
            </div>
            <div className="col-md-2">
              <Input
                className=""
                type="text"
                label="מספר בית *"
                name="houseNumber"
                placeholder=""
                ref={register({
                  required: {
                    value: true,
                    message: 'שדה חובה',
                  },
                  validate: (value) =>
                    Validations.isNumber(value) || 'אנא הזן מספר בלבד',
                })}
                error={errors.houseNumber && errors.houseNumber.message}
              />
            </div>
          </div>
          <div className="form-row">
            <div className="col-md-4">
              <Input
                className=""
                type="text"
                label="קומה"
                name="floor"
                placeholder=""
                ref={register({
                  validate: (value) =>
                    Validations.isNumber(value) || 'אנא הזן מספר בלבד',
                })}
                error={errors.floor && errors.floor.message}
              />
            </div>
            <div className="col-md-4">
              <Input
                className=""
                type="text"
                label="דירה"
                name="apartmentNumber"
                placeholder=""
                ref={register({
                  validate: (value) =>
                    Validations.isNumber(value) || 'אנא הזן מספר בלבד',
                })}
                error={errors.apartmentNumber && errors.apartmentNumber.message}
              />
            </div>
            <div className="col-md-4">
              <Input
                className=""
                type="text"
                label="מיקוד"
                name="zipCode"
                maxLength="7"
                placeholder=""
                ref={register({
                  validate: (value) =>
                    Validations.isZipCode(value) || 'מיקוד לא תקין',
                })}
                error={errors.zipCode && errors.zipCode.message}
              />
            </div>
          </div>

          <div className="form-row">
            <div className="col-md-12">
              <Textarea
                className="sm:w-full md:w-48%"
                label="הערות לשליח"
                name="deliveryNotes"
                id="deliveryNotes"
                ref={register}
              />
            </div>
          </div>
        </div>
      </Form>
      <Notification
        show={showNotification}
        handleClose={handleCloseNotification}
        content={notificationText}
      />
    </div>
  );
};

export default NewbornForm;
