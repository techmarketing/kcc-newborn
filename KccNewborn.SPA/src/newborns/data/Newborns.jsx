import React from 'react';
import { useRouteMatch, Switch, Route } from 'react-router-dom';
import './Newborns.scss';
import { Pagination, Spinner, Button, Table } from 'react-bootstrap';
import Form from '../../components/Form';
import ReactDatePicker, { registerLocale } from 'react-datepicker';
import he from 'date-fns/locale/he';
registerLocale('he', he);

const Newborns = ({
  data,
  getData,
  getExcel,
  handleMinDate,
  handleMaxDate,
  handleFilter,
  handleClearFilter,
  minDate,
  maxDate,
  isLoading = false,
  isExporting = false,
}) => {
  let { path, url } = useRouteMatch();
  const items = data.data;
  const pagination = data.pagination;

  const formatDate = (date) => {
    return new Date(date).toLocaleDateString('he-IL').replace(/[.]/g, '/');
  };

  const formatDateTime = (datetime) => {
    const dateObject = new Date(datetime);
    return `${formatDate(dateObject)} ${dateObject
      .toLocaleTimeString('he-IL', {
        hour: '2-digit',
        minute: '2-digit',
      })
      .replace(/[.]/g, '/')}`;
  };

  let active = pagination.CurrentPage;
  let total = pagination.TotalPages;
  let hasNext = pagination.HasNext;
  let hasPrevious = pagination.HasPrevious;
  let totalCount = pagination.TotalCount;

  let paginationItems = [];

  for (let n = 1; n <= total; n++) {
    paginationItems.push(
      <Pagination.Item key={n} active={n === active} onClick={() => getData(n)}>
        {n}
      </Pagination.Item>
    );
  }

  return (
    <div id="newborns" className="container table-responsive">
      <h2 className="mb-5">רשומות ניובורן</h2>

      <Switch>
        <Route exact path={path}>
          <div className="line-wrapper d-flex form-inline mb-4">
            <Form
              onSubmit={handleFilter}
              btnText="סנן"
              className="form-inline ml-3"
              btnClassName="mb-2"
              isSending={isLoading}
            >
              <ReactDatePicker
                name="minDate"
                locale="he"
                placeholderText="מתאריך"
                dateFormat="dd/MM/yyyy"
                className={`form-control mb-2 ml-sm-2`}
                onChange={(date) => handleMinDate(date)}
                selected={minDate}
                maxDate={maxDate || new Date()}
              />

              <ReactDatePicker
                name="maxDate"
                locale="he"
                placeholderText="עד תאריך"
                dateFormat="dd/MM/yyyy"
                className={`form-control mb-2 ml-sm-3`}
                onChange={(date) => handleMaxDate(date)}
                selected={maxDate}
                maxDate={new Date()}
              />
            </Form>
            <Button
              varient="primary"
              className="mb-2 ml-sm-3"
              onClick={handleClearFilter}
            >
              נקה סינון
            </Button>
            <Button
              varient="primary"
              className="mb-2 ml-sm-3"
              onClick={getExcel}
            >
              יצוא קובץ
              {isExporting && (
                <Spinner animation="border" size="sm" className="mr-2" />
              )}
            </Button>
            <div class="flex-grow-1"></div>
            <div class="ml-4">
              סה"כ <strong>{totalCount}</strong> רשומות
            </div>
          </div>

          {isLoading ? (
            <Spinner animation="border" variant="primary" />
          ) : (
            <>
              <Table bordered hover>
                <thead>
                  <tr>
                    <th>שם</th>
                    <th>מספר עובד</th>
                    <th>אתר</th>
                    <th>ילד/נכד</th>
                    <th>תאריך לידה</th>
                    <th>תאריך הזנה</th>
                    <th>הוזן ע"י</th>
                  </tr>
                </thead>
                <tbody>
                  {items.map((item, key) => {
                    const {
                      id,
                      firstName,
                      lastName,
                      employeeId,
                      site,
                      type,
                      dateOfBirth,
                      createdDate,
                      createdBy,
                    } = item;

                    return (
                      <tr key={key}>
                        <td>
                          {firstName} {lastName}
                        </td>
                        <td>{employeeId}</td>
                        <td>{site}</td>
                        <td>{type}</td>
                        <td>{formatDate(dateOfBirth)}</td>
                        <td dir="ltr">{formatDateTime(createdDate)}</td>
                        <td>{createdBy}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
              {total > 1 && (
                <div className="pagination-wrapper mt-5">
                  <Pagination className="p-0 text-center">
                    <Pagination.Prev
                      disabled={!hasPrevious}
                      onClick={() => getData('prev')}
                    />
                    {paginationItems}
                    <Pagination.Next
                      disabled={!hasNext}
                      onClick={() => getData('next')}
                    />
                  </Pagination>
                </div>
              )}
            </>
          )}
        </Route>
      </Switch>
    </div>
  );
};

export default Newborns;
