import React, { useState, useEffect } from 'react';

import { Spinner } from 'react-bootstrap';
import Newborns from './Newborns';
import NewbornsService from '../../services/newborns.service';
import ExcelService from '../../services/excel.service';

export const path = '/newborns';
export const exact = true;

export const Component = (props) => {
  const [data, setData] = useState(null);
  const [minDate, setMinDate] = useState(null);
  const [maxDate, setMaxDate] = useState(null);
  const [filter, setFilter] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [isExporting, setIsExporting] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await NewbornsService.getPagedData();
        if (response && response.data) {
          const pagination = response.headers['x-pagination'] || null;
          setData({ data: response.data, pagination: JSON.parse(pagination) });
        }
      } catch (error) {
        setData(false);
        console.log(error);
      }
    };

    fetchData();
  }, []);

  const handleMinDate = (date) => {
    setMinDate(date);
  };

  const handleMaxDate = (date) => {
    setMaxDate(date);
  };

  useEffect(() => {
    if (filter === 0) {
      return;
    }
    getData(null);
  }, [filter]);

  const handleClearFilter = () => {
    setMinDate(null);
    setMaxDate(null);
    setFilter(filter + 1);
  };

  const getExcel = async () => {
    const startDate = minDate ? formatDate(minDate) : null;
    const endDate = maxDate ? formatDate(maxDate) : null;

    try {
      setIsExporting(true);
      await ExcelService.getByDates(startDate, endDate);
      setIsExporting(false);
    } catch (error) {
      setIsExporting(false);
      console.error(error);
    }
  };

  const handleFilter = async () => {
    setIsLoading(true);
    await getData(null);
    setIsLoading(false);
  };

  const getData = async (key) => {
    const current = data.pagination.CurrentPage;
    const total = data.pagination.TotalPages;
    const pageSize = data.pagination.PageSize;

    let page = key;

    if (!key) {
      page = 1;
    } else if (key === 'prev' && current > 1) {
      page = current - 1;
    } else if (key === 'next' && current < total) {
      page = current + 1;
    }

    const startDate = minDate ? formatDate(minDate) : null;
    const endDate = maxDate ? formatDate(maxDate) : null;

    try {
      const response = await NewbornsService.getPagedData(
        page,
        pageSize,
        startDate,
        endDate
      );

      if (response && response.data) {
        const pagination = response.headers['x-pagination'] || null;
        setData({ data: response.data, pagination: JSON.parse(pagination) });
      }
    } catch (error) {
      setData(false);
      console.error(error);
    }
  };

  const formatDate = (date) => {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  };

  if (data === null) {
    return (
      <div className="spinner-container">
        <Spinner animation="border" variant="primary" />
      </div>
    );
  }

  if (data === false) {
    return <div>שגיאה</div>;
  }

  return (
    <Newborns
      data={data}
      getData={getData}
      getExcel={getExcel}
      handleMinDate={handleMinDate}
      handleMaxDate={handleMaxDate}
      handleFilter={handleFilter}
      handleClearFilter={handleClearFilter}
      minDate={minDate}
      maxDate={maxDate}
      isLoading={isLoading}
      isExporting={isExporting}
    />
  );
};
