import { render } from 'react-dom';
import React from 'react';

import App from './App';

const root = document.createElement('div');
document.body.appendChild(root);

root.setAttribute('dir', 'rtl');

root.classList.add(
  'app',
);

render(<App />, root);
