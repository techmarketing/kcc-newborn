import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from 'react-router-dom';

import './sass/main.scss';
import routes from './routes';
import Login from './login/Login';

import AuthService from './services/auth.service';
import PrivateRoute from './PrivateRoute';
import Sidebar from './components/Sidebar';

import MenuIcon from './images/menu.png';

class App extends Component {
  constructor(props) {
    super(props);
    this.updateCurrentUserState = this.updateCurrentUserState.bind(this);
    this.toggleMenu = this.toggleMenu.bind(this);
    this.menuRef = React.createRef();

    this.state = {
      currentUser: undefined,
    };
  }

  componentDidMount() {
    this.updateCurrentUserState();
  }

  updateCurrentUserState() {
    const user = AuthService.getCurrentUser();

    if (user) {
      this.setState({
        currentUser: user,
      });
    } else {
      this.setState({
        currentUser: null,
      });
    }
  }

  logOut() {
    AuthService.logout();
    this.setState({
      currentUser: null,
    });
  }

  toggleMenu() {
    if (this.menuRef.current) {
      this.menuRef.current.classList.toggle('show');
    }
  }

  render() {
    const { currentUser } = this.state;

    return (
      <Router>
        {currentUser && (
          <nav>
            <button onClick={this.toggleMenu} className="toggle-btn">
              <img src={MenuIcon} />
            </button>
          </nav>
        )}

        {currentUser && <Sidebar ref={this.menuRef} />}

        <div id="content">
          <Switch>
            <Route path="/" exact={true}>
              <Redirect to="/newborns" />
            </Route>
            <Route path="/login">
              <Login updateCurrentUserState={this.updateCurrentUserState} />
            </Route>
            {routes.map(({ Component, ...props }, index) => {
              return (
                <PrivateRoute component={Component} key={index} {...props} />
              );
            })}
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
