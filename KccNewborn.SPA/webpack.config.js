const HtmlPlugin = require('html-webpack-plugin');

module.exports = {
  output: {
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-react'],
        },
      },
      {
        test: /\.(eot|woff2?|png|jpg|jpeg|gif)$/,
        loader: 'url-loader',
      },
      {
        test: /\.(scss|css)$/,
        loaders: ['style-loader', 'css-loader', 'sass-loader']
    },
    ],
  },

  resolve: {
    extensions: ['.js', '.jsx', '.json'],
  },
  plugins: [
    new HtmlPlugin({
      title: 'קימברלי-קלארק ניו בורן',
      favicon: 'favicon.ico',
    }),
  ],
};
