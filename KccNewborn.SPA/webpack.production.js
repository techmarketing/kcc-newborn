const { merge } = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const PurgeCss = require('@fullhuman/postcss-purgecss');
const CssNano = require('cssnano');
const { resolve } = require('path');

const config = require('./webpack.config');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = merge(config, {
  output: {
    filename: '[name].[contenthash].js',
    path: resolve(__dirname, '../KccNewborn.API/wwwroot'),
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          'css-loader',
          // {
          //   loader: 'postcss-loader',
          //   options: {
          //     plugins: [
          //       PurgeCss({
          //         content: ['./src/**/*.jsx', './src/**/*.js'],
          //         defaultExtractor: (content) =>
          //           content.match(/[A-Za-z0-9-_:/]+/g) || [],
          //       }),
          //       CssNano({
          //         preset: 'default',
          //       }),
          //     ],
          //   },
          // },
        ],
      },
    ],
  },

  plugins: [
    new CleanWebpackPlugin({
      "verbose": true
    }),
    new MiniCssExtractPlugin({
      filename: '[name].[contenthash].css',
      chunkFilename: '[id].[contenthash].css',
    }),
  ],

  optimization: {
    chunkIds: 'named',
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /node_modules/,
          chunks: 'initial',
          name: 'vendor',
          priority: 10,
          enforce: true,
        },
      },
    },
  },
});