const { merge } = require('webpack-merge');
const config = require('./webpack.config');

module.exports = merge(config, {
  module: {
    rules: [
      // {
      //   test: /\.s?css$/,
      //   use: [
      //     'style-loader',
      //     'css-loader',
      //     // {
      //     //   loader: 'postcss-loader',
      //     //   options: {
      //     //     ident: 'postcss',
      //     //   },
      //     // },
      //   ],
      // },
    ],
  },
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:5000'
      }
    }
  },
});
