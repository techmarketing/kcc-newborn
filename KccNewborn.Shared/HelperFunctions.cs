﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace KccNewborn.Shared
{
    public static class HelperFunctions
    {
        public static DateTime GetIsraelDateNow()
        {
            var now = GetIsraelDateTimeNow();

            return now.Date;
        }
        public static DateTime GetIsraelDateTimeNow()
        {
            var utcNow = DateTime.UtcNow;
            TimeZoneInfo israelZone = TimeZoneInfo.FindSystemTimeZoneById("Israel Standard Time");
            return TimeZoneInfo.ConvertTimeFromUtc(utcNow, israelZone);
        }

        public static string GetEnumDescription(this Enum GenericEnum)
        {
            Type genericEnumType = GenericEnum.GetType();
            MemberInfo[] memberInfo = genericEnumType.GetMember(GenericEnum.ToString());
            if ((memberInfo != null && memberInfo.Length > 0))
            {
                var _Attribs = memberInfo[0].GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false);
                if ((_Attribs != null && _Attribs.Count() > 0))
                {
                    return ((System.ComponentModel.DescriptionAttribute)_Attribs.ElementAt(0)).Description;
                }
            }
            return GenericEnum.ToString();
        }

        public static string Sanitize(string str)
        {
            Regex rgx = new Regex(@"^\s*[a-zא-ת\-\'\s]+\s * $", RegexOptions.IgnoreCase);

            List<string> words = str.Split(' ').ToList();

            words.RemoveAll(s => s == "");

            for (int i = 0; i < words.Count; i++)
            {
                words[i] = rgx.Replace(words[i], "");
            }

            return String.Join(" ", words);
        }
    }
}
