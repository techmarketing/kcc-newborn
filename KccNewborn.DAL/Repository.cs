using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KccNewborn.Entities;
using Microsoft.EntityFrameworkCore;
using KccNewborn.Shared;

namespace KccNewborn.DAL
{
    public class Repository : IRepository
    {
        private readonly DataContext _context;
        public Repository(DataContext context)
        {
            _context = context;

        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<Newborn> GetNewborn(int id)
        {
            return await _context.Newborns.FirstOrDefaultAsync(n => n.Id == id);
        }

        public async Task<IEnumerable<Newborn>> GetNewborns(NewbornParameters parameters)
        {
            var result = await _context.Newborns.Where(n =>
                (DateTime.Compare(n.DateOfBirth, parameters.MinDateOfBirth) >= 0) &&
                (DateTime.Compare(n.DateOfBirth, parameters.MaxDateOfBirth) <= 0)).OrderByDescending(n => n.DateOfBirth).ToListAsync();

            return result;
        }

        public async Task<PagedList<Newborn>> GetPagedNewborns(NewbornParameters parameters)
        {
            var result = _context.Newborns.Where(n =>
                (DateTime.Compare(n.DateOfBirth, parameters.MinDateOfBirth) >= 0) &&
                (DateTime.Compare(n.DateOfBirth, parameters.MaxDateOfBirth) <= 0)).OrderByDescending(n => n.DateOfBirth);

            return await PagedList<Newborn>.ToPagedListAsync(result, parameters.PageNumber, parameters.PageSize);
        }

        public async Task<IEnumerable<Newborn>> GetNewbornsForDailyReport()
        {
            var yesterday = HelperFunctions.GetIsraelDateTimeNow().AddDays(-1);
            var result = await _context.Newborns.Where(n => n.CreatedDate.Date == yesterday.Date).ToListAsync();

            if (result != null && result.Any())
            {
                return result;
            }
            else
            {
                return null;
            }
        }

        public async Task<IEnumerable<Newborn>> GetNewbornsQuarterly()
        {
            var today = HelperFunctions.GetIsraelDateTimeNow();
            var threeYearsBack = today.AddYears(-3);
            var TwoAndHalfYearsBack = today.AddMonths(-30);

            return await _context.Newborns.Where(n =>
                (n.Site == SiteNames.Hadera && DateTime.Compare(n.DateOfBirth, threeYearsBack) >= 0) ||
                (n.Site != SiteNames.Hadera && DateTime.Compare(n.DateOfBirth, TwoAndHalfYearsBack) >= 0)
            ).ToListAsync();
        }

        public async Task<User> GetUser(int id)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);

            return user;
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            var users = await _context.Users.ToListAsync();

            return users;
        }

        public async Task<bool> IsEntryExists(string ident)
        {
            return await _context.DailyReports.AnyAsync(x => x.Date == DateTime.Now.Date && x.Identifier == ident);
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}