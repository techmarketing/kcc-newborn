using System.Collections.Generic;
using System.Threading.Tasks;
using KccNewborn.Entities;

namespace KccNewborn.DAL
{
    public interface IRepository
    {
         void Add<T>(T entity) where T: class;

         void Delete<T>(T entity) where T: class;

         Task<bool> SaveAll();

         Task<IEnumerable<User>> GetUsers();

         Task<User> GetUser(int id);

         Task<IEnumerable<Newborn>> GetNewborns(NewbornParameters parameters);

         Task<PagedList<Newborn>> GetPagedNewborns(NewbornParameters parameters);
         
         Task<IEnumerable<Newborn>> GetNewbornsQuarterly();

         Task<IEnumerable<Newborn>> GetNewbornsForDailyReport();

         Task<bool> IsEntryExists(string ident);

         Task<Newborn> GetNewborn(int id);
    }
}