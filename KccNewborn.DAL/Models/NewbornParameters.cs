using System;
using System.ComponentModel.DataAnnotations;
using KccNewborn.Shared;

public class NewbornParameters : QueryStringParameters
{
    public DateTime MinDateOfBirth { get; set; } = DateTime.MinValue;

    public DateTime MaxDateOfBirth { get; set; } = HelperFunctions.GetIsraelDateNow();

    public bool ValidDateRange => DateTime.Compare(MaxDateOfBirth, MinDateOfBirth) >= 0;
}