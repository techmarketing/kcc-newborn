using System.Threading.Tasks;
using KccNewborn.Entities;

namespace KccNewborn.DAL
{
    public interface IAuthRepository
    {
         Task<User> Register(User user, string password);
         Task<User> Login(string username, string password);
         Task<bool> UserExists(string username);
    }
}