using System;
using System.Collections.Generic;
using System.Linq;
using KccNewborn.Entities;
using KccNewborn.Entities.Dtos;
using Newtonsoft.Json;

namespace KccNewborn.DAL
{
    public class Seed
    {
        public static void SeedUsers(DataContext context)
        {
            if (!context.Users.Any())
            {
                var userData = System.IO.File.ReadAllText("../KccNewborn.DAL/UserSeedData.json");
                var users = JsonConvert.DeserializeObject<List<UserForRegisterDto>>(userData);

                foreach (var user in users)
                {
                    byte[] passwordHash, passwordSalt;
                    CreatePasswordHash(user.Password, out passwordHash, out passwordSalt);

                    var u = new User
                    {
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Email = user.Email.ToLower(),
                        PasswordHash = passwordHash,
                        PasswordSalt = passwordSalt
                    };

                    context.Users.Add(u);
                }

                context.SaveChanges();
            }
        }

        public static void SeedNewborns(DataContext context)
        {
            if (!context.Newborns.Any())
            {
                var newbornsData = System.IO.File.ReadAllText("../KccNewborn.DAL/NewbornsSeedData.json");
                var newborns = JsonConvert.DeserializeObject<List<Newborn>>(newbornsData);

                foreach (var newborn in newborns)
                {
                    newborn.CreatedDate = DateTime.UtcNow;
                    context.Newborns.Add(newborn);
                }

                context.SaveChanges();
            }
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
    }


}