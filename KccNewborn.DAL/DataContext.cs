﻿using System;
using KccNewborn.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace KccNewborn.DAL
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<Newborn> Newborns { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<DailyReport> DailyReports { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Newborn>()
                .Property(e => e.Site)
                .HasConversion(new EnumToStringConverter<SiteNames>());

            modelBuilder
                .Entity<Newborn>()
                .Property(e => e.Type)
                .HasConversion(new EnumToStringConverter<NewbornTypes>());
        }
    }
}
