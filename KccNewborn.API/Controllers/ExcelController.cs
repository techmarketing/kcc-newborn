using System.Threading.Tasks;
using KccNewborn.DAL;
using Microsoft.AspNetCore.Mvc;
using KccNewborn.Shared;
using Microsoft.AspNetCore.Authorization;
using KccNewborn.Core.Services;

namespace KccNewborn.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ExcelController : ControllerBase
    {
        private readonly IRepository _repo;
        private readonly IExcelService _excel;

        public ExcelController(IRepository repo, IExcelService excel)
        {
            _excel = excel;
            _repo = repo;
        }


        [HttpGet]
        public async Task<IActionResult> getByQuery([FromQuery]NewbornParameters parameters)
        {
            var result = await _repo.GetNewborns(parameters);

            var file = _excel.GenerateExcelReport(result);

            return File(
                file,
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                _excel.GenerateFileName(parameters)
            );
        }

        [HttpGet]
        [Route("quarterly")]
        public async Task<IActionResult> GetQuarterlyReport()
        {
            var result = await _repo.GetNewbornsQuarterly();

            var file = _excel.GenerateExcelReport(result);

            var fileName = $"report_{HelperFunctions.GetIsraelDateNow().ToString("dd-MM-yyyy")}.xlsx";

            return File(file, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        }
    }
}