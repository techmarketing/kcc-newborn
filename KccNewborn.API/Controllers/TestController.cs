
using KccNewborn.DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using KccNewborn.Core.Services;
using System.Threading.Tasks;
using KccNewborn.Core;
using System;

namespace KccNewborn.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IWebHostEnvironment _env;
        private readonly IEmailSender _emailSender;
        private readonly EmailSettings _emailSettings;

        public TestController(IRepository repo, IWebHostEnvironment env, IEmailSender emailSender, EmailSettings emailSettings)
        {
            _env = env;
            _emailSender = emailSender;
            _emailSettings = emailSettings;
        }

        [HttpGet]
        public async Task<IActionResult> Get(string text)
        {
            if (!String.IsNullOrWhiteSpace(text))
            {
                await _emailSender.SendEmailAsync("avi.s@techmarketing.co.il", "Test email", text);
            }
            
            return Ok(_env.EnvironmentName + " " + text);
        }
        
    }
}