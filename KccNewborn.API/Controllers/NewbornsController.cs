using System;
using System.Threading.Tasks;
using AutoMapper;
using KccNewborn.DAL;
using KccNewborn.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using KccNewborn.Shared;
using Newtonsoft.Json;

namespace KccNewborn.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class NewbornsController : ControllerBase
    {
        private readonly IRepository _repo;
        private readonly IMapper _mapper;
        private readonly UserResolver _userResolver;

        public NewbornsController(IRepository repo, IMapper mapper, UserResolver userResolver)
        {
            _mapper = mapper;
            _repo = repo;
            _userResolver = userResolver;
        }

        [HttpGet]
        public async Task<IActionResult> GetNewborns([FromQuery] NewbornParameters parameters)
        {
            if (!parameters.ValidDateRange)
            {
                return BadRequest("Max date of birth cannot be less than min date of birth");
            }

            var result = await _repo.GetPagedNewborns(parameters);

            var metadata = new
            {
                result.TotalCount,
                result.PageSize,
                result.CurrentPage,
                result.TotalPages,
                result.HasNext,
                result.HasPrevious
            };

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetNewborn(int id)
        {
            var newborn = await _repo.GetNewborn(id);

            var a = HelperFunctions.GetEnumDescription(newborn.Site);

            return Ok(newborn);
        }

        [HttpPost]
        public async Task<IActionResult> CreateNewborn(Newborn newborn)
        {
            newborn.CreatedDate = HelperFunctions.GetIsraelDateTimeNow();
            newborn.CreatedBy = await _userResolver.GetUserFullName();

            _repo.Add(newborn);

            if (await _repo.SaveAll())
            {
                var newbornToReturn = _mapper.Map<Newborn>(newborn);

                return Ok(newbornToReturn);
            }

            return BadRequest("Failed to create Newborn");
        }
    }
}