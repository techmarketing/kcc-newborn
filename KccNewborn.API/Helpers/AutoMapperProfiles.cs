using System;
using AutoMapper;
using KccNewborn.Entities.Dtos;
using KccNewborn.Entities;

namespace KccNewborn.API.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<User, UserForListDto>(); 
        }
    }
}