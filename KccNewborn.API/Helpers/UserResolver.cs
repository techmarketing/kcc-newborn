using System.Security.Claims;
using System.Threading.Tasks;
using KccNewborn.DAL;
using KccNewborn.Entities;
using Microsoft.AspNetCore.Http;

public class UserResolver
{
    private readonly IHttpContextAccessor _httpContext;
    private readonly IRepository _repo;
    public UserResolver(IHttpContextAccessor httpContext, IRepository repo)
    {
        _httpContext = httpContext;
        _repo = repo;
    }

    public async Task<string> GetUserFullName()
    {
        var claim = _httpContext.HttpContext.User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;
        var user = await _repo.GetUser(int.Parse(claim));

        if (user == null)
        {
            return null;
        }

        return $"{user.FirstName} {user.LastName}";


    }
}